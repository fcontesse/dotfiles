#!/bin/bash

kill $(pgrep rofi)
rofi -show run 16 | xargs swaymsg exec
